# bounce.py
#
# Exercise 1.5

ball_height = 100
bounce_portion = 3 / 5
for bounce in range(1, 11):
    ball_height = round((ball_height * bounce_portion), 4)
    print("After", bounce, "bounces, the ball is", ball_height, "meters high.")
